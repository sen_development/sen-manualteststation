﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.Description" Type="Str">This driver configures and takes measurements form the RIGOL DG 1000 series Function/Arbitrary Waveform Generator</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*#!!!*Q(C=\&gt;3^4=.1&amp;)&lt;B$U2"GQV16DALJ+/A1"%&amp;%O6J0%!+GJ2?Y;S1&amp;&lt;,#73&amp;6[G3&amp;]0L[+)3#J#%33.CZRP\OX_.L9[FN^^+&gt;$J?W\OQWV$.5.RY`TTIOBE/LL[MP:RL0DPUV2CU^8H&gt;@_J]?ON0_Y_`&lt;_=`\&gt;8&lt;\L`_P`^PV&amp;T&lt;;,#]WOP)GX41J*3F"=9KVOTME?:)H?:)H?:)(?:!(?:!(?:!\O:-\O:-\O:-&lt;O:%&lt;O:%&lt;O:'XH6TE)B=ZKZ*-HES5$*I-E$3'IO31?"*0YEE]8#LR**\%EXA3$UW5?"*0YEE]C9&gt;O3DS**`%EHM4$5%W3&lt;3@(EXA98I%H]!3?Q".YG&amp;+"*Q!%EQ5$"Y0!5&amp;!:H!3?Q".Y/&amp;8A#4S"*`!%(KI6?!*0Y!E]A9=O&lt;67C;9;&gt;(!`$S0%Y(M@D?"Q01]PR/"\(YXA=$^0*]4A?"_&amp;-[!Q/15YHJY&amp;TY8A=$X`E?"S0YX%]DI?K^I3]L=SA'8:S0)&lt;(]"A?QW.Y'%+'R`!9(M.D?"B7BM@Q'"\$9XC93I&lt;(]"A?!W*-SP1S"D-['IW-Q0$Q;W_,N;=547*NLX`.]566P9#K&amp;UPVQKB?".5$6DUYV1.2,&lt;2K!65,I\JBV9WIA+K*61/K'GL0=5.:5V;5HL+AT#ETSJ1S'&lt;L_=-0^@K`.:K0V?KX6;K7_\\69,$3@TT7&lt;T43&gt;4D7:4)Z@KQ@W=&lt;MZ`3ZN__XL\PXN:@PY`,4L8F^W8!`ZEH&lt;8+O.X[4&gt;]'X7LQ]FHHD8[!)*\2$U!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.1.1.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Action-Status" Type="Folder">
			<Item Name="MeasureVoltageMAX.vi" Type="VI" URL="../Public/Action-Status/MeasureVoltageMAX.vi"/>
			<Item Name="MeasureVoltageMIN.vi" Type="VI" URL="../Public/Action-Status/MeasureVoltageMIN.vi"/>
			<Item Name="MeasureCurrentMAX.vi" Type="VI" URL="../Public/Action-Status/MeasureCurrentMAX.vi"/>
			<Item Name="MeasureCurrentMIN.vi" Type="VI" URL="../Public/Action-Status/MeasureCurrentMIN.vi"/>
			<Item Name="MeasureResistance.vi" Type="VI" URL="../Public/Action-Status/MeasureResistance.vi"/>
			<Item Name="MeasurePower.vi" Type="VI" URL="../Public/Action-Status/MeasurePower.vi"/>
			<Item Name="GetInputState.vi" Type="VI" URL="../Public/Configure/GetInputState.vi"/>
			<Item Name="GetTriggerSource.vi" Type="VI" URL="../Public/Configure/GetTriggerSource.vi"/>
			<Item Name="GetOperationMode.vi" Type="VI" URL="../Public/Configure/GetOperationMode.vi"/>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="EnableInput.vi" Type="VI" URL="../Public/Configure/EnableInput.vi"/>
			<Item Name="SetVoltage.vi" Type="VI" URL="../Public/Configure/SetVoltage.vi"/>
			<Item Name="SetCurrent.vi" Type="VI" URL="../Public/Configure/SetCurrent.vi"/>
			<Item Name="SetResistance.vi" Type="VI" URL="../Public/Configure/SetResistance.vi"/>
			<Item Name="SetPower.vi" Type="VI" URL="../Public/Configure/SetPower.vi"/>
			<Item Name="SetOperationMode.vi" Type="VI" URL="../Public/Configure/SetOperationMode.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="Delay.vi" Type="VI" URL="../Public/Utility/Delay.vi"/>
			<Item Name="Reset.vi" Type="VI" URL="../Public/Utility/Reset.vi"/>
			<Item Name="Self-Test.vi" Type="VI" URL="../Public/Utility/Self-Test.vi"/>
			<Item Name="Error Query.vi" Type="VI" URL="../Public/Utility/Error Query.vi"/>
			<Item Name="Revision Query.vi" Type="VI" URL="../Public/Utility/Revision Query.vi"/>
		</Item>
		<Item Name="Initialize.vi" Type="VI" URL="../Public/Initialize.vi"/>
		<Item Name="Close.vi" Type="VI" URL="../Public/Close.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="../Public/VI Tree.vi"/>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Write With Delay.vi" Type="VI" URL="../Private/Write With Delay.vi"/>
	</Item>
</Library>
