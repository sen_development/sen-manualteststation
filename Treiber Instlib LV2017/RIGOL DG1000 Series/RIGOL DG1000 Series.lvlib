<?xml version='1.0'?>
<Library LVVersion="8208000">
   <Property Name="Instrument Driver" Type="Str">True</Property>
   <Property Name="NI.Lib.DefaultMenu" Type="Str">dir.mnu</Property>
   <Property Name="NI.Lib.Description" Type="Str">This driver configures and takes measurements form the RIGOL DG 1000 series Function/Arbitrary Waveform Generator</Property>
   <Property Name="NI.Lib.HelpPath" Type="Str"></Property>
   <Property Name="NI.Lib.Icon" Type="Bin">###!!!!!!!Y!%%!$#GFN97&gt;F)(2Z='5!!""!!QNJ&lt;7&amp;H:3"E:8"U;!!%!!5!%E"!!!(`````!!)&amp;;7VB:W5!%E"!!!(`````!!)%&lt;7&amp;T;Q!!"!!(!"2!1!!"`````Q!&amp;"G.P&lt;'^S=Q!!#E!#"'RF:H1!!!B!!A.U&lt;X!!#E!#"8*J:WBU!!R!!A:C&lt;X2U&lt;WU!!"B!5!!%!!=!#!!*!!I*5G6D&gt;'&amp;O:WRF!$A!]1!!!!!!!!!"$7FN97&gt;F:'&amp;U93ZD&gt;'Q!)E"1!!9!!!!"!!-!"!!'!!M+37VB:W5A:'&amp;U91!!#A"1!!)!$!!-!!%!$1!!!!!!!!!)!!!%!0```````````````````````````````````````````WNL;WNL;WNL;WNL;WNL;WNL;WNL;WNL;WNL;WNL;```;WM!!!"L;WM!!!"L!!!!;WNL!!!!;WM!;Q"L;Q"L``^L;Q"L;Q"L!'NL;WM!;WM!;Q"L;WNL!!"L!'M!;WP``WNL!!!!;WM!;Q!!;Q"L;Q"L!'M!!'NL!'M!!'NL;```;WM!;WM!;Q"L;Q"L!'NL!'M!;WM!;WM!;Q"L!'NL``^L;Q"L;Q"L;Q!!!'M!!!"L;WM!!!"L;Q"L!'NL!'P``WNL;WNL;WNL;WNL;WNL;WNL;WNL;WNL;WNL;WNL;```!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!#!``````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````]!!!%!!0```Q$``]Q!``_:!0``:A$``T-!``]!!0`-`Q$`T-Q!`]S:!0`-:A$`T$-!`]Q!!0_:`Q$`G=Q!`ZG:!0_::A$`G4-!`ZE!!0^G`Q$`:MQ!`W;:!0^G:A$`:D-!`W9!!0]T`Q$`-]Q!`T/:!0]T:A$`-T-!`T-!!0]!`Q$`!-Q!`Q#:!0]!:A$`!$-!`Q!!!-T``Q$-`]Q!T0_:!-T`:A$-`T-!T0]!!-T-`Q$-T-Q!T-S:!-T-:A$-T$-!T-Q!!-S:`Q$-G=Q!T*G:!-S::A$-G4-!T*E!!-RG`Q$-:MQ!T';:!-RG:A$-:D-!T'9!!-QT`Q$--]Q!T$/:!-QT:A$--T-!T$-!!-Q!`Q$-!-Q!T!#:!-Q!:A$-!$-!T!!!!*H``Q#:`]Q!G@_:!*H`:A#:`T-!G@]!!*H-`Q#:T-Q!G=S:!*H-:A#:T$-!G=Q!!*G:`Q#:G=Q!G:G:!*G::A#:G4-!G:E!!*FG`Q#::MQ!G7;:!*FG:A#::D-!G79!!*ET`Q#:-]Q!G4/:!*ET:A#:-T-!G4-!!*E!`Q#:!-Q!G1#:!*E!:A#:!$-!G1!!!'&lt;``Q"G`]Q!:P_:!'&lt;`:A"G`T-!:P]!!'&lt;-`Q"GT-Q!:MS:!'&lt;-:A"GT$-!:MQ!!';:`Q"GG=Q!:JG:!';::A"GG4-!:JE!!':G`Q"G:MQ!:G;:!':G:A"G:D-!:G9!!'9T`Q"G-]Q!:D/:!'9T:A"G-T-!:D-!!'9!`Q"G!-Q!:A#:!'9!:A"G!$-!:A!!!$0``Q!T`]Q!-`_:!$0`:A!T`T-!-`]!!$0-`Q!TT-Q!-]S:!$0-:A!TT$-!-]Q!!$/:`Q!TG=Q!-ZG:!$/::A!TG4-!-ZE!!$.G`Q!T:MQ!-W;:!$.G:A!T:D-!-W9!!$-T`Q!T-]Q!-T/:!$-T:A!T-T-!-T-!!$-!`Q!T!-Q!-Q#:!$-!:A!T!$-!-Q!!!!$``Q!!`]Q!!0_:!!$`:A!!`T-!!0]!!!$-`Q!!T-Q!!-S:!!$-:A!!T$-!!-Q!!!#:`Q!!G=Q!!*G:!!#::A!!G4-!!*E!!!"G`Q!!:MQ!!';:!!"G:A!!:D-!!'9!!!!T`Q!!-]Q!!$/:!!!T:A!!-T-!!$-!!!!!`Q!!!-Q!!!#:!!!!:A!!!$-!\A!!!.U!!!#\!!!!KA!!!)A!!!"X!!!!61!!!%1!!!!C!!!!%1!!!!$O!!!!X1!!!,M!!!#K!!!!C!!!!(=!!!"6!!!!2!!!!#)!!!!2!!!!!/Y!!!$&gt;!!!!OQ!!!+I!!!#)!!!!&gt;Q!!!&amp;5!!!"%!!!!)A!!!"%!\O\O!.X&gt;X1#\O\M!KKKK!)C)C!"X&gt;X=!6666!%2%2!!C)C)!%2%2!!!!!!!!!!!!)!!A!!!!!!!!!!%!!!#!```````````DC/.&lt;\8N?6_.,5U`N;VN8\9DD7`````_!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!@````]!!!#!``````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````]!!!!#!0```Q!!!!!!!!!!!#!!)!!!!!!</Property>
   <Property Name="NI.Lib.Version" Type="Str">1.1.1.0</Property>
   <Item Name="Public" Type="Folder">
      <Property Name="NI.LibItem.Scope" Type="Int">1</Property>
      <Item Name="Action-Status" Type="Folder">
         <Item Name="Low Level" Type="Folder">
            <Item Name="Action-Status_Low Level.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Low Level/Action-Status_Low Level.mnu"/>
            <Item Name="Send Software Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Low Level/Send Software Trigger.vi"/>
         </Item>
         <Item Name="Action-Status.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Action-Status.mnu"/>
         <Item Name="Clear Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Clear Arbitrary Waveform.vi"/>
         <Item Name="Clear Memory Location.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Clear Memory Location.vi"/>
         <Item Name="Enable Output.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Enable Output.vi"/>
         <Item Name="Enable Phase Align.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Enable Phase Align.vi"/>
      </Item>
      <Item Name="Configure" Type="Folder">
         <Item Name="Low Level" Type="Folder">
            <Item Name="Configure_Low Level.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Low Level/Configure_Low Level.mnu"/>
            <Item Name="Configure Trigger Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Low Level/Configure Trigger Delay.vi"/>
            <Item Name="Configure Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Low Level/Configure Trigger.vi"/>
         </Item>
         <Item Name="Configure.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure.mnu"/>
         <Item Name="Configure Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Arbitrary Waveform.vi"/>
         <Item Name="Configure Burst .vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Burst .vi"/>
         <Item Name="Configure Coupling.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Coupling.vi"/>
         <Item Name="Configure Frequency Sweep.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Frequency Sweep.vi"/>
         <Item Name="Configure Function.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Function.vi"/>
         <Item Name="Configure Memory Location.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Memory Location.vi"/>
         <Item Name="Configure Modulation (Amplitude).vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Modulation (Amplitude).vi"/>
         <Item Name="Configure Modulation (Frequency).vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Modulation (Frequency).vi"/>
         <Item Name="Configure Modulation (FSK).vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Modulation (FSK).vi"/>
         <Item Name="Configure Modulation (Phase).vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Modulation (Phase).vi"/>
         <Item Name="Configure Modulation.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Modulation.vi"/>
         <Item Name="Configure Output Advanced.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Output Advanced.vi"/>
         <Item Name="Configure Output.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Output.vi"/>
         <Item Name="Configure Phase Angle.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Phase Angle.vi"/>
         <Item Name="Configure Pulse.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Pulse.vi"/>
         <Item Name="Configure Standard Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Standard Waveform.vi"/>
         <Item Name="Configure Sweep.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Sweep.vi"/>
         <Item Name="Configure Voltage.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Voltage.vi"/>
         <Item Name="Configure Voltage Advanced.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Voltage Advanced.vi"/>
         <Item Name="Create Decimal Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Create Decimal Arbitrary Waveform.vi"/>
         <Item Name="Create Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Create Arbitrary Waveform.vi"/>
         <Item Name="Delete Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Delete Arbitrary Waveform.vi"/>
         <Item Name="Rename Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Rename Arbitrary Waveform.vi"/>
      </Item>
      <Item Name="Utility" Type="Folder">
         <Item Name="Utility.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Utility.mnu"/>
         <Item Name="Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Delay.vi"/>
         <Item Name="Error Query.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Error Query.vi"/>
         <Item Name="Reset.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Reset.vi"/>
         <Item Name="Revision Query.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Revision Query.vi"/>
         <Item Name="Self-Test.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Self-Test.vi"/>
      </Item>
      <Item Name="dir.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/dir.mnu"/>
      <Item Name="Close.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Close.vi"/>
      <Item Name="Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Initialize.vi"/>
      <Item Name="VI Tree.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/VI Tree.vi"/>
   </Item>
   <Item Name="Private" Type="Folder">
      <Property Name="NI.LibItem.Scope" Type="Int">2</Property>
      <Item Name="Scale Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Private/Scale Arbitrary Waveform.vi"/>
      <Item Name="Write With Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Private/Write With Delay.vi"/>
   </Item>
   <Item Name="RIGOL DG1000 Series Readme.html" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/RIGOL DG1000 Series Readme.html"/>
</Library>
