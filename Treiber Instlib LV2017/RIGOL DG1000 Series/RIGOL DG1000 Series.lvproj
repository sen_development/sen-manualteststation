<?xml version='1.0'?>
<Project Type="Project" LVVersion="8208000">
   <Property Name="Instrument Driver" Type="Str">True</Property>
   <Property Name="NI.Project.Description" Type="Str">This project is used by developers to edit API and example files for LabVIEW Plug and Play instrument drivers.</Property>
   <Item Name="My Computer" Type="My Computer">
      <Property Name="CCSymbols" Type="Str">OS,Win;CPU,x86;</Property>
      <Property Name="specify.custom.address" Type="Bool">false</Property>
      <Item Name="Examples" Type="Folder">
         <Item Name="RIGOL DG1000 Series Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Examples/RIGOL DG1000 Series Arbitrary Waveform.vi"/>
         <Item Name="RIGOL DG1000 Series Frequency Sweep.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Examples/RIGOL DG1000 Series Frequency Sweep.vi"/>
         <Item Name="RIGOL DG1000 Series Software Triggered Frequency Sweep.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Examples/RIGOL DG1000 Series Software Triggered Frequency Sweep.vi"/>
         <Item Name="RIGOL DG1000 Series Standard Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Examples/RIGOL DG1000 Series Standard Waveform.vi"/>
         <Item Name="RIGOL DG1000 Series Triggered Burst Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Examples/RIGOL DG1000 Series Triggered Burst Waveform.vi"/>
         <Item Name="RIGOL DG1000 Series.bin3" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Examples/RIGOL DG1000 Series.bin3"/>
      </Item>
      <Item Name="RIGOL DG1000 Series.lvlib" Type="Library" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/RIGOL DG1000 Series.lvlib">
         <Item Name="Public" Type="Folder">
            <Item Name="Action-Status" Type="Folder">
               <Item Name="Low Level" Type="Folder">
                  <Item Name="Action-Status_Low Level.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Low Level/Action-Status_Low Level.mnu"/>
                  <Item Name="Send Software Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Low Level/Send Software Trigger.vi"/>
               </Item>
               <Item Name="Action-Status.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Action-Status.mnu"/>
               <Item Name="Clear Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Clear Arbitrary Waveform.vi"/>
               <Item Name="Clear Memory Location.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Clear Memory Location.vi"/>
               <Item Name="Enable Output.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Enable Output.vi"/>
               <Item Name="Enable Phase Align.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Enable Phase Align.vi"/>
            </Item>
            <Item Name="Configure" Type="Folder">
               <Item Name="Low Level" Type="Folder">
                  <Item Name="Configure_Low Level.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Low Level/Configure_Low Level.mnu"/>
                  <Item Name="Configure Trigger Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Low Level/Configure Trigger Delay.vi"/>
                  <Item Name="Configure Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Low Level/Configure Trigger.vi"/>
               </Item>
               <Item Name="Configure.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure.mnu"/>
               <Item Name="Configure Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Arbitrary Waveform.vi"/>
               <Item Name="Configure Burst .vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Burst .vi"/>
               <Item Name="Configure Coupling.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Coupling.vi"/>
               <Item Name="Configure Frequency Sweep.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Frequency Sweep.vi"/>
               <Item Name="Configure Function.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Function.vi"/>
               <Item Name="Configure Memory Location.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Memory Location.vi"/>
               <Item Name="Configure Modulation (Amplitude).vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Modulation (Amplitude).vi"/>
               <Item Name="Configure Modulation (Frequency).vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Modulation (Frequency).vi"/>
               <Item Name="Configure Modulation (FSK).vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Modulation (FSK).vi"/>
               <Item Name="Configure Modulation (Phase).vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Modulation (Phase).vi"/>
               <Item Name="Configure Modulation.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Modulation.vi"/>
               <Item Name="Configure Output Advanced.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Output Advanced.vi"/>
               <Item Name="Configure Output.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Output.vi"/>
               <Item Name="Configure Phase Angle.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Phase Angle.vi"/>
               <Item Name="Configure Pulse.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Pulse.vi"/>
               <Item Name="Configure Standard Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Standard Waveform.vi"/>
               <Item Name="Configure Sweep.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Sweep.vi"/>
               <Item Name="Configure Voltage.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Voltage.vi"/>
               <Item Name="Configure Voltage Advanced.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Voltage Advanced.vi"/>
               <Item Name="Create Decimal Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Create Decimal Arbitrary Waveform.vi"/>
               <Item Name="Create Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Create Arbitrary Waveform.vi"/>
               <Item Name="Delete Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Delete Arbitrary Waveform.vi"/>
               <Item Name="Rename Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Rename Arbitrary Waveform.vi"/>
            </Item>
            <Item Name="Utility" Type="Folder">
               <Item Name="Utility.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Utility.mnu"/>
               <Item Name="Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Delay.vi"/>
               <Item Name="Error Query.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Error Query.vi"/>
               <Item Name="Reset.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Reset.vi"/>
               <Item Name="Revision Query.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Revision Query.vi"/>
               <Item Name="Self-Test.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Self-Test.vi"/>
            </Item>
            <Item Name="dir.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/dir.mnu"/>
            <Item Name="Close.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Close.vi"/>
            <Item Name="Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Initialize.vi"/>
            <Item Name="VI Tree.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/VI Tree.vi"/>
         </Item>
         <Item Name="Private" Type="Folder">
            <Item Name="Scale Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Private/Scale Arbitrary Waveform.vi"/>
            <Item Name="Write With Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Private/Write With Delay.vi"/>
         </Item>
         <Item Name="RIGOL DG1000 Series Readme.html" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/RIGOL DG1000 Series Readme.html"/>
      </Item>
      <Item Name="Dependencies" Type="Dependencies"/>
      <Item Name="Build Specifications" Type="Build"/>
   </Item>
</Project>
