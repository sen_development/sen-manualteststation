﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.DefaultMenu" Type="Str">dir.mnu</Property>
	<Property Name="NI.Lib.Description" Type="Str">This driver configures and takes measurements form the RIGOL DG 1000 series Function/Arbitrary Waveform Generator</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*#!!!*Q(C=\&gt;3^4=.1&amp;)&lt;B$U2"GQV16DALJ+/A1"%&amp;%O6J0%!+GJ2?Y;S1&amp;&lt;,#73&amp;6[G3&amp;]0L[+)3#J#%33.CZRP\OX_.L9[FN^^+&gt;$J?W\OQWV$.5.RY`TTIOBE/LL[MP:RL0DPUV2CU^8H&gt;@_J]?ON0_Y_`&lt;_=`\&gt;8&lt;\L`_P`^PV&amp;T&lt;;,#]WOP)GX41J*3F"=9KVOTME?:)H?:)H?:)(?:!(?:!(?:!\O:-\O:-\O:-&lt;O:%&lt;O:%&lt;O:'XH6TE)B=ZKZ*-HES5$*I-E$3'IO31?"*0YEE]8#LR**\%EXA3$UW5?"*0YEE]C9&gt;O3DS**`%EHM4$5%W3&lt;3@(EXA98I%H]!3?Q".YG&amp;+"*Q!%EQ5$"Y0!5&amp;!:H!3?Q".Y/&amp;8A#4S"*`!%(KI6?!*0Y!E]A9=O&lt;67C;9;&gt;(!`$S0%Y(M@D?"Q01]PR/"\(YXA=$^0*]4A?"_&amp;-[!Q/15YHJY&amp;TY8A=$X`E?"S0YX%]DI?K^I3]L=SA'8:S0)&lt;(]"A?QW.Y'%+'R`!9(M.D?"B7BM@Q'"\$9XC93I&lt;(]"A?!W*-SP1S"D-['IW-Q0$Q;W_,N;=547*NLX`.]566P9#K&amp;UPVQKB?".5$6DUYV1.2,&lt;2K!65,I\JBV9WIA+K*61/K'GL0=5.:5V;5HL+AT#ETSJ1S'&lt;L_=-0^@K`.:K0V?KX6;K7_\\69,$3@TT7&lt;T43&gt;4D7:4)Z@KQ@W=&lt;MZ`3ZN__XL\PXN:@PY`,4L8F^W8!`ZEH&lt;8+O.X[4&gt;]'X7LQ]FHHD8[!)*\2$U!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.1.1.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Action-Status" Type="Folder">
			<Item Name="Low Level" Type="Folder">
				<Item Name="Action-Status_Low Level.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Low Level/Action-Status_Low Level.mnu"/>
				<Item Name="Send Software Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Low Level/Send Software Trigger.vi"/>
			</Item>
			<Item Name="Action-Status.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Action-Status.mnu"/>
			<Item Name="Clear Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Clear Arbitrary Waveform.vi"/>
			<Item Name="Clear Memory Location.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Clear Memory Location.vi"/>
			<Item Name="Enable Output.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Enable Output.vi"/>
			<Item Name="Enable Phase Align.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Enable Phase Align.vi"/>
			<Item Name="Counter Get Level.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Counter Get Level.vi"/>
			<Item Name="Counter Get Coupling.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Counter Get Coupling.vi"/>
			<Item Name="Counter Get Sensitive.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Counter Get Sensitive.vi"/>
			<Item Name="Counter Get State.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Counter Get State.vi"/>
			<Item Name="Counter Measure.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Action-Status/Counter Measure.vi"/>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="Low Level" Type="Folder">
				<Item Name="Configure_Low Level.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Low Level/Configure_Low Level.mnu"/>
				<Item Name="Configure Trigger Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Low Level/Configure Trigger Delay.vi"/>
				<Item Name="Configure Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Low Level/Configure Trigger.vi"/>
			</Item>
			<Item Name="Configure.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure.mnu"/>
			<Item Name="Configure Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Arbitrary Waveform.vi"/>
			<Item Name="Configure Burst .vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Burst .vi"/>
			<Item Name="Configure Coupling.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Coupling.vi"/>
			<Item Name="Configure Frequency Sweep.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Frequency Sweep.vi"/>
			<Item Name="Configure Function.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Function.vi"/>
			<Item Name="Configure Memory Location.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Memory Location.vi"/>
			<Item Name="Configure Modulation (Amplitude).vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Modulation (Amplitude).vi"/>
			<Item Name="Configure Modulation (Frequency).vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Modulation (Frequency).vi"/>
			<Item Name="Configure Modulation (FSK).vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Modulation (FSK).vi"/>
			<Item Name="Configure Modulation (Phase).vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Modulation (Phase).vi"/>
			<Item Name="Configure Modulation.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Modulation.vi"/>
			<Item Name="Configure Output Advanced.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Output Advanced.vi"/>
			<Item Name="Configure Output.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Output.vi"/>
			<Item Name="Configure Phase Angle.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Phase Angle.vi"/>
			<Item Name="Configure Pulse.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Pulse.vi"/>
			<Item Name="Configure Standard Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Standard Waveform.vi"/>
			<Item Name="Configure Sweep.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Sweep.vi"/>
			<Item Name="Configure Voltage.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Voltage.vi"/>
			<Item Name="Configure Voltage Advanced.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Voltage Advanced.vi"/>
			<Item Name="Create Decimal Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Create Decimal Arbitrary Waveform.vi"/>
			<Item Name="Create Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Create Arbitrary Waveform.vi"/>
			<Item Name="Delete Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Delete Arbitrary Waveform.vi"/>
			<Item Name="Rename Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Rename Arbitrary Waveform.vi"/>
			<Item Name="Configure Counter Auto.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Counter Auto.vi"/>
			<Item Name="Configure Counter Coupling.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Counter Coupling.vi"/>
			<Item Name="Configure Counter sensitive.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Counter sensitive.vi"/>
			<Item Name="Configure Counter State.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Counter State.vi"/>
			<Item Name="Configure Counter Level.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Configure/Configure Counter Level.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="Utility.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Utility.mnu"/>
			<Item Name="Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Delay.vi"/>
			<Item Name="Error Query.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Error Query.vi"/>
			<Item Name="Reset.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Reset.vi"/>
			<Item Name="Revision Query.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Revision Query.vi"/>
			<Item Name="Self-Test.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Utility/Self-Test.vi"/>
		</Item>
		<Item Name="dir.mnu" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/dir.mnu"/>
		<Item Name="Close.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Close.vi"/>
		<Item Name="Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/Initialize.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Public/VI Tree.vi"/>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Scale Arbitrary Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Private/Scale Arbitrary Waveform.vi"/>
		<Item Name="Write With Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/Private/Write With Delay.vi"/>
	</Item>
	<Item Name="RIGOL DG1000 Series Readme.html" Type="Document" URL="/&lt;instrlib&gt;/RIGOL DG1000 Series/RIGOL DG1000 Series Readme.html"/>
</Library>
